# Wait, you're on the Authors page, right?
Yes, you are on the Authors page of our team blog. Feel free

## Current Authors
| Authors | Profile Page | Their Commits On GitLab |
| ------- | ------------ | ----------------------- |
| **Andrei Jiroh Eugenio Halili** | [View page generated by Mkdocs](AndreiJirohHaliliDev2006) or [by GitLab itself](https://gitlab.com/MadeByThePinsTeam-DevLabs/group-site-blog/blob/master/authors/AndreiJirohHaliliDev2006.md) | `Soon` |
