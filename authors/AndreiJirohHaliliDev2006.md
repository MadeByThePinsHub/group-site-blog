# About the Author: Andrei Jiroh
## Author's Basic Info
| Metadata | Data Content |
| -------- | ------------ |
| **Short Bio** | An otaku, Node.js coder and team founder/lead at the Pins team. |
| **Joined since** | Founding date on Feburary 24, 2018 |
| **GitLab** | @AndreiJirohHaliliDev2006 |
| **GitHub** | [@AndreiJirohHaliliDev2006](https://github.com/AndreiJirohHaliliDev2006) |
| **Keybase** | [@ajhalili2006](https://keybase.io/ajhalilidev2006) |
| **Twitter** | [@kuys_potpot](https://twitter.com/kuys_potpot)

## Latest Posts
Come back in the future, maybe the author is unavailable to write posts for now, hehehe.
