# Blog

## For team members
To update the blog, you need to push changes to an seperate GitLab repository. [See the docs for details](https://en.handbooksbythepins.cf/life-at-the-pins/maintaining-main-website/blog).